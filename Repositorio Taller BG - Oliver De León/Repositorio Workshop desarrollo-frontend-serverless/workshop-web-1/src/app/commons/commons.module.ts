import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { ListGroupComponent } from './list-group/list-group.component';



@NgModule({
  declarations: [
    NavbarComponent,
    ListGroupComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    NavbarComponent,
    ListGroupComponent
  ]
})
export class CommonsModule { }
