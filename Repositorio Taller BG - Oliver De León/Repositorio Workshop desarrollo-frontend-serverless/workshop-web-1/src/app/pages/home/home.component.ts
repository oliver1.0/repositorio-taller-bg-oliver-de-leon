import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/commons/interfaces/user';
import { GetusersService } from 'src/app/services/http/getusers.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  User!: User[];

  constructor(private getUserService: GetusersService) { }

  ngOnInit(): void {
    this.getUserService.getUsers()
    .subscribe((response: User[]) => {

      // Logica especifica del componente
      response.forEach(r => {
        console.log(`Desde el Componente | Ciudad: ${r.address?.city}`);
      });

      this.User = response;

    });
  }

  userSelected(user: User): void {
    console.log(user.id);
  }

}

