// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  //usersEndpoint: "https://jsonplaceholder.typicode.com",
  lambdaUserEndpoint: "https://cyfnxeyaj7ay5vfcowma4m73he0sxlmz.lambda-url.us-east-1.on.aws",

  lambdaMessageService: "https://"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
