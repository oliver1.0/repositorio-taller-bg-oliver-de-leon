
use bd_test

drop table bd_test.cliente_ODL
CREATE TABLE bd_test.cliente_ODL (
id_cliente int NOT NULL AUTO_INCREMENT,
nombre varchar(200) NOT NULL,
apellido varchar(64) NOT NULL,
cedula varchar(64) NOT NULL,
sexo char(1) NULL,
fecha_nacimiento datetime NOT NULL,
PRIMARY KEY (id_cliente),
UNIQUE INDEX idx_unique_cedula(cedula)
)

drop table bd_test.cliente_HOBBIEODL

use bd_test
CREATE TABLE bd_test.cliente_hobbie_ODL (
id_cliente int NOT NULL ,
id_hobbie int NOT NULL ,
PRIMARY KEY (id_cliente,id_hobbie)
)
comment 'Tabla de hobbies de clientes de Oliver De León';

CREATE TABLE bd_test.hobbie_ODL (
id_hobbie int NOT NULL AUTO_INCREMENT,
descripcion varchar(64) NOT NULL,
PRIMARY KEY (id_hobbie)
)
comment 'Tabla categoria de hobbies de clientes de Oliver De León';





