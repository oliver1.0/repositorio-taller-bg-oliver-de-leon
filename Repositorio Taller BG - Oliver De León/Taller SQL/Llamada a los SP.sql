set @o_return = 0;
/*Insertar un registro*/   
call bd_test.sp_cliente('C1', 'Alfred','pol','8-74-1471','M','1987-05-17', @o_return);
/*Consulta por id de cliente*/
call bd_test.sp_cliente('R1', 1000, null, null, null, null, null, @o_return);
/*Consulta por cédula*/
call bd_test.sp_cliente('R2', null, null, null, '8-789-132', null, null, @o_return);
/*Consulta 1000 registros*/
call bd_test.sp_cliente('R3', null, null, null, null, null, null, @o_return);

/*Actualiza Registro*/
call bd_test.sp_cliente('R1', 4000, null, null, null, null, null, @o_return); /*Verifico valores anteriores*/
call bd_test.sp_cliente('U1', 4000, 'Jacinto', 'Rodríguez', 'E-810-9478', 'M', '1981-01-12', @o_return); /*Actualizo valores*/
call bd_test.sp_cliente('R1', 4000, null, null, null, null, null, @o_return); /*Verifico nuevos valores*/

/*Agregar hobbies al cliente*/
CALL bd_test.sp_cliente_hobbie('C1',1000,3, null, @o_return);
CALL `bd_test`.`sp_consulta_cliente_hobbies`(1000);

/*select @o_return;*/
