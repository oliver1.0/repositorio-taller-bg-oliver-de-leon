use bd_test; 
drop procedure if exists sp_consulta_cliente_hobbies ; 

delimiter $$

create procedure sp_consulta_cliente_hobbies (i_id_cliente int)
begin

select *
from bd_test.cliente a, 
     bd_test.cliente_hobbie b, 
	 bd_test.hobbie c
where a.id_cliente = b.id_cliente
  and b.id_hobbie = c.id_hobbie
  and a.id_cliente = i_id_cliente;
  
end$$

delimiter ; 
