use bd_test;

insert into bd_test.cliente_ODL (nombre,apellido,cedula,sexo,fecha_nacimiento) values ('Elias','Pérez','8-77-1471','M','1987-07-17');
insert into bd_test.cliente_ODL (nombre,apellido,cedula,sexo,fecha_nacimiento) values ('Olivia','Díaz','PE-12-1575','F','1974-10-18');
insert into bd_test.cliente_ODL (nombre,apellido,cedula,sexo,fecha_nacimiento) values ('Carla','Prieto','8-209-1728','F','1988-10-22');
use bd_test;

insert into  bd_test.hobbie_ODL (descripcion) values ('Crucigramas ');
insert into  bd_test.hobbie_ODL (descripcion) values ('Baile ');
insert into  bd_test.hobbie_ODL (descripcion) values ('Programar ');

insert into bd_test.cliente_hobbie_ODL (id_cliente, id_hobbie) values (1,2);
insert into bd_test.cliente_hobbie_ODL (id_cliente, id_hobbie) values (2,3);
insert into bd_test.cliente_hobbie_ODL (id_cliente, id_hobbie) values (3,1);
