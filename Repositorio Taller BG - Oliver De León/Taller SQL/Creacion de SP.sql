USE `bd_test`;
DROP procedure IF EXISTS `sp_cliente`;

DELIMITER $$
/*USE `bd_test`$$*/
CREATE PROCEDURE `sp_cliente` (
IN i_operacion char(2),
IN i_id_cliente int,
IN i_nombre varchar (64),
IN i_apellido varchar (64),
IN i_cedula varchar(64),
IN i_sexo char(1),
IN i_fecha_nacimiento datetime,
OUT o_return int
)
sp_quit:
BEGIN

set o_return = 0;

if (i_operacion = 'C1') then 
    insert into bd_test.cliente (id_cliente,   nombre,   apellido,   cedula,   sexo,   fecha_nacimiento) 
	             	     values (i_id_cliente, i_nombre, i_apellido, i_cedula, i_sexo, i_fecha_nacimiento);

end if;


if (i_operacion = 'R1') then
select id_cliente,
	   nombre,
	   apellido,
	   cedula,
	   sexo,
       fecha_nacimiento 
from bd_test.cliente
where id_cliente = i_id_cliente;
end if;


if (i_operacion = 'R2') then
select id_cliente,
	   nombre,
	   apellido,
	   cedula,
	   sexo,
       fecha_nacimiento 
from bd_test.cliente
where cedula = i_cedula;
end if;


if (i_operacion = 'R3') then
select 
       id_cliente,
	   nombre,
	   apellido,
	   cedula,
	   sexo,
       fecha_nacimiento 
from bd_test.cliente limit 1000;

end if;

if ( i_operacion = 'U1') then 
update bd_test.cliente
set    nombre = i_nombre,
	   apellido = i_apellido,
	   cedula = i_cedula,
	   sexo = i_sexo,
       fecha_nacimiento = i_fecha_nacimiento
where id_cliente = i_id_cliente;
end if;

if (i_operacion = 'D1' ) then 
delete from db_test.cliente
where id_cliente = i_id_cliente;
end if;

END$$
DELIMITER ;
