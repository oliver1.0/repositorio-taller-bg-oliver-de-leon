Taller Docker


Link del contenedor en Docker hub

docker pull oliverxd/mi-ubuntu-python:1.1


Abrimos el Playground y creamos una nueva instancia

![Crear nueva instancia](image.png)


# Revisar versión de docker
$ docker --version

![Alt text](image-1.png)

# Intentamos correr el comando hello-world

![helloworld](image-2.png)


# instalamos ubunto e ingresamos a la terminal 

$ docker run -it ubuntu bash

![ubuntu](image-3.png)

#corremos ubuntu en el contenedor 

$docker run -ti ubuntu /bin/bash

![Alt text](image-4.png)

#instalamos figlet 

$apt-get install figlet

![install_figlet](image-5.png)

# crearmos el hola mundo en figlet

$figlet "Hola mundo :D"

![Holamundofiglet](image-6.png)

# Listamos las imagenes que tenemos creadas en docker

$ docker image ls 

y para ordenar desde la ultima creacion 

$ docker ps -a | head 

![imagenesendocker](image-7.png)

# realizamos un commit a la ultima imagen creada y le pones la renombramos con el nombre miubuntu:1.0

$ docker commit id

$ docker image tag miubuntu:1.0

![Alt text](image-8.png)


# descargamos un nuevo contenedor de nginx y los iniciamos en el puerto 8080

$ docker run -d --rm -p 8000:80 --name mi-nginx nginx

![nginx](image-9.png)

![Alt text](image-10.png)

# revisamos los procesos 

$ docker ps 

# y observamos los logs para que ver que esa saliendo mal

$ docker logs mi-nginx

![logs](image-11.png)

# visualizamos el sistema de archivos dentro del contenedor 

$ docker exec mi-nginx ls

![sistemaa de archivos](image-12.png)

# eliminamos el contenedor anterior

docker rm [id_docker] -f

# creamos una carpeta llamada html

$ mkdir html

y dentro de esta creamos un index.html

$ touch index.html

![html](image-13.png)

# nos vamos al edior web y colocamos el siguiente codigo 

<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Mi primera pagina en docker</title>
</head>

<body>
  <h1>Bienvenido a mi primera web desde docker</h1>
  <h2> Hola mundo :) </h2>
  
</body>
</html>

![index](image-14.png)


# cambiamos los permisos de la carpeta para que pueda ser accesdida desde el nginx

$ chmod -R 777 *

# Corremos una nueva instancia del docker pasandole como parámetro el volumen de la maquina host a la máquina 

$ docker run -d -p 8000:80 -v /root/html:/usr/share/nginx/html --name mi-nginx nginx

![corriendo el index en nginx](image-15.png)

![mi pagina docker](image-16.png)

# Crear una nueva red llamada mi-red utilizando bride
$ docker network create -d bridge mi-red

# procedemos a ver si se creo satifactoriamente 

$ docker network ls 

# e inspeccionamos la red 

$ docker network inspect mi-red

![red docker](image-17.png)

# creamos otro docker de nginx, pero utilizara la red que acabamos de crear 

$ docker run -d -p 8082:80 --net=mi-red --name mi-nginx-3 nginx

![red](image-18.png)

![red](image-19.png)